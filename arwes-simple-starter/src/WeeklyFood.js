import React, { useEffect, useState } from "react";

const WeeklyFood = () => {
    var d = new Date();
var weekday = new Array(7);
weekday[0] = "Söndag";
weekday[1] = "Måndag";
weekday[2] = "Tisdag";
weekday[3] = "Onsdag";
weekday[4] = "Torsdag";
weekday[5] = "Fredag";
weekday[6] = "Lördag";

var food = new Array(7);
food[0] = "Köttbullar med potatismos";
food[1] = "Fisknuggets och ris";
food[2] = "Blodpudding med bacon och lingonsylt";
food[3] = "Pastasallad";
food[4] = "Pannkakor och ärtsoppa";
food[5] = "Fläskkarré med äppelmos";
food[6] = "Fiskbullar och kokt potatis";

var todaysFood = food[d.getDay()];
var tomorrowsFood = food[(d.getDay() + 1)]

var day = weekday[d.getDay()];
  return (
    <div>
      <h5>Matsedel</h5>
      <p><strong>Idag:</strong> {todaysFood}</p>
      <p><strong>Imorgon:</strong> {tomorrowsFood}</p>
    </div>
  );
};

export default WeeklyFood;
