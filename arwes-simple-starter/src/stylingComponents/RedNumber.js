import React from "react";

const RedNumber = (number) => {
  return <span style={{ color: "red" }}>{number.children}</span>;
};

export default RedNumber;
