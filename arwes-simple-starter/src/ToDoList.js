import React, { useEffect, useState } from "react";

const ToDOList = () => {
  return (
    <div>
      <h5>Att göra</h5>
      <p>- Åka till återvinningen</p>
      <p>- Köpa filter till ventilerna</p>
      <p>- Skapa matsedel för nästa vecka</p>
    </div>
  );
};

export default ToDOList;
