import React, { useEffect } from "react";
import { ThemeProvider, createTheme, Row, Col, Arwes, Frame } from "arwes";
import moment from "moment";
import Clock from "react-live-clock";
import Weather from "./Weather";
import Internet from "./Internet";
import WeeklyFood from "./WeeklyFood";
import ToDoList from "./ToDoList";
import Calendar from "./Calendar";
import TvSport from "./TvSport";

// const styles = theme => ({
//   root: {
//     padding: [theme.padding, 0],
//     background: theme.background.primary.level0
//   }
// });

const firstRowBox = { padding: 10 };

const App = () => (
  <div>
    <ThemeProvider theme={createTheme()} style={{ backgroundColor: "red" }}>
      <div id="app-background">
        <Arwes animate show>
          {anim => (
            <div style={{ padding: 20, textAlign: "center" }}>
              <Row style={{ marginBottom: 0 }}>
                <Col s={2}>
                  <img
                    src="https://i.pinimg.com/originals/14/30/1c/14301c7116f1ef503964cbfff0241a04.gif"
                    style={{
                      height: "80px",
                      width: "80px",
                      borderRadius: "50px 50px 50px 50px"
                    }}
                  />
                </Col>
                <Col s={8}>
                  <h3>Engström Karlssons Dashboard</h3>
                </Col>
                <Col s={2}>
                  <h3>
                    <Clock
                      format={"HH:mm:ss"}
                      ticking={true}
                      timezone={"Europe/Stockholm"}
                    />
                  </h3>
                </Col>
              </Row>
              <Row style={{ marginBottom: "0" }}>
                <Col s={4}>
                  <div
                    style={{ padding: "20px", maxWidth: 400, margin: "0 auto" }}
                  >
                    <Frame animate={true} level={3} corners={4} layer="primary">
                      <div style={firstRowBox}>
                        <Weather />
                      </div>
                    </Frame>
                  </div>
                </Col>
                <Col s={4}>
                  <div
                    style={{ padding: "20px", maxWidth: 400, margin: "0 auto" }}
                  >
                    <Frame animate={true} level={3} corners={4} layer="primary">
                      <div style={firstRowBox}>
                        <WeeklyFood />
                      </div>
                    </Frame>
                  </div>
                </Col>
                <Col s={4}>
                  <div
                    style={{ padding: "20px", maxWidth: 400, margin: "0 auto" }}
                  >
                    <Frame animate={true} level={3} corners={4} layer="primary">
                      <div style={firstRowBox}>
                        <Internet />
                      </div>
                    </Frame>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col s={12}>
                  <Frame animate={true} level={3} corners={4} layer="primary">
                    <div style={firstRowBox}>
                      <Calendar />
                    </div>
                  </Frame>
                </Col>
              </Row>
              <Row>
                <Col s={4}>
                  <Frame animate={true} level={3} corners={4} layer="primary">
                    <div style={firstRowBox}>
                      <TvSport />
                    </div>
                  </Frame>
                </Col>
                <Col s={4}></Col>
                <Col s={4}>
                  <Frame animate={true} level={3} corners={4} layer="primary">
                    <div style={firstRowBox}>
                      <ToDoList />
                    </div>
                  </Frame>
                </Col>
              </Row>
            </div>
          )}
        </Arwes>
      </div>
    </ThemeProvider>
  </div>
);

export default App;
