import React, { useState, useEffect } from "react";
import moment from "moment";

const RoutineEvents = (who) => {

    const [events, setEvents] = useState([]);

    const dadArray = [
        ["Slutar jobbet 16:30"],
        ["Slutar jobbet 16:30", "Träning 18.00-20.00"],
        ["Slutar jobbet 16:30"],
        ["Slutar jobbet 16:30", "Träning 18.00-20.00"],
        ["Slutar jobbet 16.00"],
        ["Helg!"],
        ["Helg!"]
    ];

    const momArray = [
        ["Slutar jobbet 16:00"],
        ["Slutar jobbet 16:00"],
        ["Slutar jobbet 16:00"],
        ["Slutar jobbet 15:00"],
        ["Slutar jobbet 15.00"],
        ["Helg!"],
        ["Helg!"]
    ]

    const ianArray = [
        ["Pappa lämnar på förskolan 07:45", "Mamma hämtar på förskolan 16:30"],
        ["Pappa lämnar på förskolan 07:45", "Pappa hämtar på förskolan 16:30"],
        ["Pappa lämnar på förskolan 07:45", "Pappa hämtar på förskolan 16:30"],
        ["Mamma lämnar på förskolan 07:30", "Mamma hämtar på förskolan 15:30"],
        ["Mamma lämnar på förskolan 07:30", "Mamma hämtar på förskolan 15:30"],
        ["Helg!"],
        ["Helg!"]
    ]

    useEffect(() => {
        getEvents();
      }, [events > 0]);

    const todaysDate = moment().day();
    const todaysDateString = moment().format('dddd');

    const getEvents = () => {

        console.log(dadArray[todaysDate - 1])
    switch(who.who) {
        case "Pappa":
            setEvents(dadArray[todaysDate - 1])
            break;
        case "Mamma":
            setEvents(momArray[todaysDate - 1])
            break;
        case "Ian":
            setEvents(ianArray[todaysDate - 1])
            break;
        default: setEvents(null);
    }
    }
    console.log(events)

    return (
        <div>
            {events.map(function(item, i){
            return <p key={i}>{item}</p>
            })}
        </div>
    )

}

export default RoutineEvents;