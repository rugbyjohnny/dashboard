import React, { useEffect, useState } from "react";
import * as SportConsts from "./Constants";
import moment from "moment";

const TvSport = () => {
  const todaysDate = moment().format("YYYY-MM-DD");
  const [games, setGames] = useState([]);
  const [teams, setTeams] = useState([]);

  useEffect(() => {
    getTeams();
    getSportsToday();
  }, [teams.length > 0]);

  const getTeams = () => {
    var teamsArray = [];
    teamsArray.push(SportConsts.Tottenam, SportConsts.Warriors, SportConsts.Färjestad, SportConsts.GambaOsaka, SportConsts.Hammarby)
    setTeams(teamsArray);
  }

  const getSportsToday = () => {
    var eventsArray = [];
    teams.map((team) => {
    
    fetch(
      SportConsts.CORSPROXY + "https://www.thesportsdb.com/api/v1/json/1/eventsnext.php?id=" +
        team,
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        }
      }
    )
      .then(res => res.json())
      .then(result => {
        if(result.events){
          console.log(result.events[0]);
          if (result.events[0].dateEvent === todaysDate) {
            eventsArray.push(result.events[0]);
          }
        }
      });
    })
    setGames(eventsArray);
  };

  const eventStyle = { width: "40%" };
  const timeResultStyle = { float: "right" };
  return (
    <div>
      <h5>Tv & Sport</h5>
      {games.length > 0 ? games.map((event) =>
         <div>
         <span style={eventStyle}>
           <strong>{event.strLeague}:</strong><br/>
            {event.strEvent}
         </span>
         <p style={timeResultStyle}>
           {event.strResult ? event.strResult : moment(event.strTime, 'hh:mm:ss').add(1, 'hours').format('HH:mm')}
         </p>
       </div>
      ) : <p>Inga matcher idag</p>}
     </div>
  );
};

export default TvSport;
