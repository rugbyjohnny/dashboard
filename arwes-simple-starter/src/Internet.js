import React, { useEffect, useState } from "react";
import NetworkSpeed from "network-speed";

const Internet = () => {
    const testNetworkSpeed = new NetworkSpeed();
  const [internetInfo, setInternetInfo] = useState("");

  useEffect(() => {
    const interval = setInterval(() => {
        
    getInternetInfo();
      }, 3000);
      return () => clearInterval(interval);
    
  },);

  const getInternetInfo = async() => {
    const baseUrl = 'http://eu.httpbin.org/stream-bytes/50000000';
    const fileSizeInBytes = 5000000;
    const speed = await testNetworkSpeed.checkDownloadSpeed(baseUrl, fileSizeInBytes);
    setInternetInfo(speed.mbps);
    }
    
    var effective_type = navigator.connection.effectiveType;

  return (
    <div>
      <h5>Internethastighet</h5>
      <p>{internetInfo} M/bs</p>
      <p>Comhem {effective_type}</p>
    </div>
  );
};

export default Internet;
