import React, { useEffect, useState } from "react";
import RedNumber from "./stylingComponents/RedNumber";
import { CORSPROXY } from "./Constants";
import CloudMov from "../src/assets/weatherIcons/CLOUD.mov";
import SunMov from "./assets/weatherIcons/SUN.mp4";
import RainMov from "./assets/weatherIcons/SUN_RAIN.mp4";

const Weather = () => {
  const [currentWeather, setCurrentWeather] = useState("");
  const [degrees, setDegrees] = useState(0);
  const [feelsLike, setFeelsLike] = useState(0);
  const [wind, setWind] = useState(0);
  const [imgWeather, setImgWeather] = useState(null);
  const [vidWeather, setVidWeather] = useState(null);

  useEffect(() => {
    getWeather();
  }, [currentWeather]);

  const getWeather = () => {
    fetch(
      CORSPROXY + "http://api.openweathermap.org/data/2.5/weather?q=Stockholm&appid=dd75ae269c8b8eebbad72c7102fdaf69&lang=se",
      {
        headers: {
          "Content-Type": "application/json",
          'Access-Control-Allow-Origin': "*",
          "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
          "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token",
          Accept: "application/json"
        }
      }
    )
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        var desc = result.weather[0].description;
        setCurrentWeather(desc.charAt(0).toUpperCase() + desc.slice(1));
        var kelvinNow = result.main.temp;
        setDegrees(Math.round(kelvinNow - 273.15));
        var kelvinFeelsLike = result.main.feels_like;
        setFeelsLike(Math.round(kelvinFeelsLike - 273.15));
        setImgWeather(
          "http://openweathermap.org/img/wn/" +
            result.weather[0].icon +
            "@2x.png"
        );
        
         let videoTag = "";
         console.log(result.weather[0].icon)
         switch(result.weather[0].icon) {
            case "Clouds":
              videoTag = CloudMov
              break;
            case "Sun":
              videoTag = SunMov
              break;
            case "Rain":
              videoTag = RainMov;
              break;
            default:
              videoTag = SunMov
          }
        setVidWeather(videoTag)
        
        setWind(result.wind.speed);
      });
  };

  return (
    <div>
      <h5>Vädret idag</h5>
      <div className="weather-container">
        <div>
          <span>{currentWeather}</span>
          <span style={{ float: "right" }}>
            {/* <img alt="" src={imgWeather} /> */}
            {/* <img alt="" style={{width: "5rem", height: "5rem;"}}src={clouds} /> */}
            <video style={{width: "7rem", height: "7rem;"}} autostart autoPlay loop src={vidWeather} type="video/mp4" />
          </span>
        </div>
        <p>{degrees} celsius</p>
        <p>Känns som {feelsLike} celcius</p>
        <p>
          Blåser{" "}
          {wind > 5 ? <RedNumber>{wind}</RedNumber> : <span>{wind}</span>} meter
          i sekunden
        </p>
      </div>
    </div>
  );
};

export default Weather;
