import React, { useEffect, useState } from "react";
import * as GAPI from "./Constants";
import { gapi } from 'gapi-script';
import moment from 'moment';
import RoutineEvents from './RoutineEvents';

const Calendar = () => {
  const url = 'https://www.googleapis.com/calendar/v3/calendars/jengstrom27@gmail.com/events';
  const clientId = '472166249642-i02e0mpfij8ntusnu5912gord4e28qil.apps.googleusercontent.com';
  const clientSecret = 'HRu8TnMoesG7nd_gM4C5gu3i';
  // fetch(url).then(
  //   res => res.json()
  // ).then(result => {
  //   console.log(result)
  // })

  // useEffect(() => {
  //   authenticate();
  //   loadClient();
  //   execute();
  // }, []);

  const authenticate = () => {
    return gapi.auth2.getAuthInstance()
        .signIn({scope: "https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.events https://www.googleapis.com/auth/calendar.events.readonly https://www.googleapis.com/auth/calendar.readonly"})
        .then(function() { console.log("Sign-in successful"); },
              function(err) { console.error("Error signing in", err); });
  }

  const loadClient = () => {
    gapi.client.setApiKey(GAPI.GoogleApiClient);
    return gapi.client.load("https://content.googleapis.com/discovery/v1/apis/calendar/v3/rest")
        .then(function() { console.log("GAPI client loaded for API"); },
              function(err) { console.error("Error loading GAPI client for API", err); });
  }

  const execute = () => {
    return gapi.client.calendar.events.list({
      "calendarId": "jengstrom27@gmail.com"
    })
        .then(function(response) {
                // Handle the results here (response.result has the parsed body).
                console.log("Response", response);
              },
              function(err) { console.error("Execute error", err); });
  }
  gapi.load("client:auth2", function() {
    gapi.auth2.init({client_id: GAPI.GoogleApiClient});
  });
  return (
    <div>
      <h5>Kalender idag {moment().format("DD/MM/YYYY")}</h5>
      <table style={{margin: '0 auto', width: '100%'}}>
          <td style={{width: '30%'}}>
          <th style={{display: 'block', border: '#26dafd 1px solid'}}>Pappa</th>
          <tr>
            <RoutineEvents who="Pappa"/>
          </tr>
          </td>
          <td style={{width: '30%'}}>
          <th style={{display: 'block', border: '#26dafd 1px solid'}}>Mamma</th>
          <tr>
              <RoutineEvents who="Mamma" />
          </tr>
          </td>
          <td style={{width: '30%'}}>
          <th style={{display: 'block',border: '#26dafd 1px solid'}}>
              Ian
          </th>
          <tr>
            <RoutineEvents who="Ian"/>
          </tr>
          </td>
      </table>
    </div>
  );
};

export default Calendar;
